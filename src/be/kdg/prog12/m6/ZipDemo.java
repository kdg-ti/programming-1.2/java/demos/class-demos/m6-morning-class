package be.kdg.prog12.m6;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ZipDemo {
    public static void main(String[] args) {
        Path path = Paths.get("/home/lars/tmp/screenshots.zip");
        try (DataInputStream dis = new DataInputStream(
                new BufferedInputStream(
                        new FileInputStream(path.toFile())))) {
            dis.skip(26);
            short fileNameLength = dis.readByte(); // Little-endian :-/
            dis.readByte(); // second byte is going to be zeroes (I hope)
            System.out.println("FILE NAME LENGTH: " + fileNameLength);
            dis.skip(2); // Now I will be at offset 30
            String fileName = new String(dis.readNBytes(fileNameLength));
            System.out.println("FILE NAME: " + fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
