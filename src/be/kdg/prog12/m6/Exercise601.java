package be.kdg.prog12.m6;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Exercise601 {
    public static void main(String[] args) {
        Path dir = Paths.get("module6");
        if(!Files.exists(dir)){
            try{
                Files.createDirectory(dir);
            }catch(IOException e){
                System.out.println(e.getMessage());
            }
            System.out.println("I've created a dir.");
        }
        else{
            System.out.println("The dir already exists.");
        }

        Path path = dir.resolve("my_file.txt");
        // OR:    Path path = Paths.get("module6/my_file.txt");
        if (!Files.exists(path)) {
            try {
                Path file = Files.createFile(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (Files.exists(path)) {
            List<String> someText = List.of("Ihsan", "Hepsen");
            try {
                Files.write(path, someText);
            } catch (IOException e) {
                System.out.println("Writing failed");
            }
        } else {
            System.out.println("File does not exist!");
        }

        /*try {
            Map<String, Object> attrs = Files.readAttributes(path, "*");
            for (Map.Entry<String, Object> entry : attrs.entrySet()) {
                System.out.printf("%s %s%n", entry.getKey(), entry.getValue());
            }
        } catch (IOException e) {
            System.out.println("Reading attrs. failed!");
        }*/

        try {
            Map<String, Object> metadata = Files.readAttributes(path, "*");
            List<String> metadata1 = new ArrayList<>();
            for (Map.Entry<String, Object> str : metadata.entrySet()) {
                /*metadata1.add*/System.out.println(str.getValue() + "   " + str.getKey());
            }
            //System.out.println(metadata1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        /*try {
            Map<String, Object> metadata = Files.readAttributes(path, "*");
            System.out.printf("%5s %s", metadata.keySet().toString(), metadata.values());
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        // TODO: final steps of the exercise...
        Path path2 = Paths.get("some_copy.txt");
        try {
            Files.copy(path, path2);
        } catch (IOException e) {

        }
    }
}
