package be.kdg.prog12.m6;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class TextDemo {
    public static void main(String[] args) throws IOException {
        File file = new File("third.txt");
        // Character streams will end with 'Reader' or 'Writer' (as opposed to 'Stream')
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line = br.readLine();
            while (line != null) {
                System.out.println("LINE: " + line);
                line = br.readLine();
            }
        }

        try (Scanner scanner =  new Scanner(new FileInputStream(file))) {
            while (scanner.hasNext()) {
                String line = scanner.nextLine();
                System.out.println("LINE: " + line);
            }
        }

        File file2 = new File("third.txt");
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file2))) {
            bw.write("Hello world");
            bw.newLine();
            bw.write("This is a demo");
        }
    }
}
